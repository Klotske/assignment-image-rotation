//
// Created by ilya on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"
#include "stdint.h"
#include "stdio.h"


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_MEMORY_ERROR,
    READ_ERROR
};

enum read_status from_bmp(FILE *in, struct image *img_p);

enum read_status read_from_bmp(struct image *img_p, char *path);

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE *out, struct image img);

enum write_status write_to_bmp(struct image img, char *path);

#endif  //ASSIGNMENT_IMAGE_ROTATION_BMP_H
