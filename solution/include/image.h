//
// Created by ilya on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include "stdint.h"

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel* get_pixel(struct pixel* data_ptr, uint64_t width, uint64_t x, uint64_t y);

struct image create_image();

void free_image(struct image img);

#endif  //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
