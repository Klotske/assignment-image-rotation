//
// Created by ilya on 30.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#include "image.h"

struct image rotate(struct image orig);

#endif  //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
