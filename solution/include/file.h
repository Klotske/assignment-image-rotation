//
// Created by ilya on 29.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H
#include "stdio.h"

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum open_status open_file(FILE** file, char* path, char* mode);

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum close_status close_file(FILE* file);

#endif  //ASSIGNMENT_IMAGE_ROTATION_FILE_H
