#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotate.h"
#include "stdint.h"

int main(int argc, char** argv) {
    if (argc != 3) {
        // Print error in stderr
        printf("Check argument count!");
        return 1;
    }

    struct image img = create_image();

    enum read_status rStatus = read_from_bmp(&img, argv[1]);
    if (rStatus) {
        // Print error in stderr
        printf("Reading failed! Error code: %d", rStatus);
        free_image(img);
        return 2;
    }

    struct image rotated = rotate(img);

    enum write_status wStatus = write_to_bmp(rotated, argv[2]);
    if (wStatus) {
        // Print error in stderr
        printf("Writing failed! Error code: %d", wStatus);
        free_image(img);
        free_image(rotated);
        return 3;
    }

    // Cleanup
    free_image(img);
    free_image(rotated);
    return 0;
}
