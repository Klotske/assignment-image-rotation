//
// Created by ilya on 28.12.2021.
//
#include "bmp.h"

#include <stdio.h>
#include <stdlib.h>

#include "file.h"
#include "image.h"

#define BM_SIGNATURE 0x4D42
#define PIX_PER_METER 2834

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPixelsPerMeter;
    uint32_t biYPixelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static uint8_t get_padding(uint64_t width) {
    if ((width * 3) % 4 == 0)
        return 0;
    else {
        return 4 - ((width * 3) % 4);
    }
}

static struct bmp_header get_header(struct image img) {
    uint32_t image_size = img.width * img.height * sizeof(struct pixel) + (img.height + 1) * get_padding(img.width);
    return (struct bmp_header) {
        .bfType = BM_SIGNATURE,
        .bfileSize = image_size + sizeof(struct bmp_header),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = img.width,
        .biHeight = img.height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = image_size,
        .biXPixelsPerMeter = PIX_PER_METER,
        .biYPixelsPerMeter = PIX_PER_METER,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

enum read_status from_bmp(FILE *in, struct image *img_p) {
    struct bmp_header header = {0};
    size_t result = fread(&header, sizeof(struct bmp_header), 1, in);

    if (result != 1)
        return READ_INVALID_HEADER;

    if (header.bfType != BM_SIGNATURE)
        return READ_INVALID_SIGNATURE;

    result = fseek(in, header.bOffBits, SEEK_SET);

    if (result)
        return READ_ERROR;

    struct pixel *pix = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
    if (!pix) {
        free(pix);
        return READ_MEMORY_ERROR;
    }

    img_p->width = header.biWidth;
    img_p->height = header.biHeight;

    uint8_t padding = get_padding(img_p->width);

    for (uint64_t i = 0; i < header.biHeight; i++) {
        result = fread(
                get_pixel(pix, header.biWidth, 0, i),
            sizeof(*pix),
            header.biWidth,
            in);
        if (result != header.biWidth)
            return READ_INVALID_BITS;

        result = fseek(in, padding, SEEK_CUR);
        if (result)
            return READ_ERROR;
    }

    img_p->data = pix;

    return READ_OK;
}

enum read_status read_from_bmp(struct image *img_p, char *path) {
    FILE *fBMP = {0};
    enum open_status openStatus = open_file(&fBMP, path, "r");
    if (openStatus == OPEN_OK) {
        enum read_status readStatus = from_bmp(fBMP, img_p);

        if (close_file(fBMP) == CLOSE_ERROR)
            return READ_ERROR;

        return readStatus;
    }
    return READ_ERROR;
}

enum write_status to_bmp(FILE *out, struct image img) {
    struct bmp_header header = get_header(img);
    size_t result = fwrite(&header, sizeof(struct bmp_header), 1, out);

    if (result != 1)
        return WRITE_ERROR;

    result = fseek(out, header.bOffBits, SEEK_SET);

    if (result)
        return WRITE_ERROR;

    uint8_t padding = get_padding(img.width);

    uint32_t zero = 0;

    for (uint64_t i = 0; i < img.height; i++) {
        result = fwrite(get_pixel(img.data, img.width, 0, i), sizeof(struct pixel), img.width, out);
        if (result != img.width)
            return WRITE_ERROR;
        result = fwrite(&zero, 1, padding, out);
        if (result != padding)
            return WRITE_ERROR;
    }

    return WRITE_OK;
}

enum write_status write_to_bmp(struct image img, char *path) {
    FILE *fBMP = {0};
    enum open_status openStatus = open_file(&fBMP, path, "w+");
    if (openStatus == OPEN_OK) {
        enum write_status writeStatus = to_bmp(fBMP, img);

        if (close_file(fBMP) == CLOSE_ERROR)
            return WRITE_ERROR;

        return writeStatus;
    }
    return WRITE_ERROR;
}
