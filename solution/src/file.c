//
// Created by ilya on 29.12.2021.
//
#include "file.h"

#include <stdio.h>

enum open_status open_file(FILE** file, char* path, char* mode) {
    *file = fopen(path, mode);
    if (*file)
        return OPEN_OK;
    else
        return OPEN_ERROR;
}

enum close_status close_file(FILE* file) {
    int result = fclose(file);
    if (result != EOF)
        return CLOSE_OK;
    else
        return CLOSE_ERROR;
}
