//
// Created by ilya on 29.12.2021.
//
#include "../include/image.h"

#include <stdlib.h>

struct pixel* get_pixel(struct pixel* data_ptr, uint64_t width, uint64_t x, uint64_t y) {
    return &data_ptr[width * y + x];
}

struct image create_image() {
    return (struct image) {0};
}

void free_image(struct image img) {
    free(img.data);
}
