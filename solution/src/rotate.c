//
// Created by ilya on 30.12.2021.
//
#include "../include/rotate.h"

#include <stdlib.h>

#include "../include/image.h"

struct image rotate(struct image orig) {
    struct image rotated = create_image();
    rotated.height = orig.width;
    rotated.width = orig.height;

    rotated.data = malloc(rotated.width * rotated.height * sizeof(struct pixel));

    for (uint64_t h = 0; h < orig.height; h++) {
        for (uint64_t w = 0; w < orig.width; w++) {
            rotated.data[(rotated.width - h - 1) + w * rotated.width] = *get_pixel(orig.data, orig.width, w, h);;
        }
    }

    return rotated;
}
